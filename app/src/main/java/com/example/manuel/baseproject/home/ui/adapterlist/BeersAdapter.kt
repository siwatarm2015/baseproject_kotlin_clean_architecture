package com.example.manuel.baseproject.home.ui.adapterlist

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.manuel.baseproject.R
import com.example.manuel.baseproject.home.ui.adapterlist.model.BeerAdapterModel
import com.example.manuel.baseproject.home.ui.adapterlist.viewholder.BeerViewHolder

class BeersAdapter(private var beers: List<BeerAdapterModel>, private val context: Context) :
        RecyclerView.Adapter<BeerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder =
            BeerViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.item_list_beer, parent, false)
            )


    override fun getItemCount(): Int = beers.size

    override fun onBindViewHolder(viewHolder: BeerViewHolder, position: Int) {
        viewHolder.apply {
            val abv = beers[position].abv.toString()
            val formattedAbv: String = context.resources.getString(R.string.abv, abv)

            beerAbvTextView.text = formattedAbv
            beerNameTextView.text = beers[position].name
            beerTaglineTextView.text = beers[position].tagline

            Glide.with(context)
                    .load(beers[position].image)
                    .placeholder(R.drawable.ic_close_black)
                    .override(200, 300)
                    .into(beerImageTextView)


            val backgroundColor = ContextCompat.getColor(context, beers.get(position).abvColor)
            (beerAbvTextView.background as GradientDrawable).setColor(backgroundColor)
        }
    }

    fun updateAdapter(updatedList: List<BeerAdapterModel>) {
        val result = DiffUtil.calculateDiff(BeersDiffCallback(this.beers, updatedList))

        this.beers = updatedList.toMutableList()
        result.dispatchUpdatesTo(this)
    }

    fun getBeers(): List<BeerAdapterModel> = beers
}
